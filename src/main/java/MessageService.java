public class MessageService {

    public static  boolean sendMessage(String ip, String message){

        if(Network.sendToRemoteIP(ip,message)){
            return true;
        }else{
            //retry
            return Network.retry(ip, message);
        }
    }

}
