import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MessageServiceTest {

    @Test
    public void ifSendMessageFailsAtFirst() {
        when(Network.sendToRemoteIP("1.2.3.4","this is a message")).thenReturn(false);
        assertTrue(MessageService.sendMessage("1.2.3.4","this is a message"));
    }

    @Test
    public void ifSendMessageFailsonRetry() {
        when(Network.sendToRemoteIP("1.2.3.4","this is a message")).thenReturn(true);
        when(Network.sendToRemoteIP("1.2.3.4","this is a message")).thenReturn(false);
        assertTrue(MessageService.sendMessage("1.2.3.4","this is a message"));
    }

}